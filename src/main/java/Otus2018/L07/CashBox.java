// -*- coding: utf-8; indent-tabs-mode: nil; word-wrap: t; mode: java; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-07-16 10:50:45 korskov>

package L07;

import L07.BankNote;

/**
 * кассета для банкнот для ATM
 */
public interface CashBox {
    /**
     * извлекает запрошенную сумму из кассеты;
     * возвращает null при невозможности выдать запрошенную сумму;
     */
    public BankNote[] get(int amount);

    /**
     * помещает данную сумму в кассету
     */
    public boolean put(BankNote[] cash);

    /**
     * возвращает общую сумму в кассете
     */
    public int getTotal();
}
