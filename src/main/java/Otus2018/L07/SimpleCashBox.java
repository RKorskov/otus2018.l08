// -*- coding: utf-8; indent-tabs-mode: nil; word-wrap: t; mode: java; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-07-16 12:26:06 korskov>

package L07;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import L07.BankNote;

public class SimpleCashBox implements CashBox {
    private HashMap <Integer, BankNote> 飞块;

    SimpleCashBox() {
        飞块 = new HashMap <Integer, BankNote> ();
    }

    SimpleCashBox(BankNote[] cash1st) {
        this();
        for(BankNote 块 : cash1st)
            飞块.put(块.getNominal(), 块);
    }

    /**
     * извлекает запрошенную сумму из кассеты;
     * возвращает null при невозможности выдать запрошенную сумму;
     * извлечение идёт от большего номинала к меньшему.
     */
    public BankNote[] get(final int amount) {
        if(amount < 1 || 飞块.isEmpty())
            return null;
        int rem = amount;
        ArrayList <BankNote> reqb = new ArrayList <BankNote> (飞块.size());
        Integer[] 块毛 = 飞块.keySet().toArray(new Integer[0]);
        Arrays.sort(块毛, (a, b) -> (a == b ? 0 : (a > b ? -1 : 1))); // desc.
        for(int cnom : 块毛) {
            if(cnom > rem)
                continue;
            int acash = 飞块.get(cnom).getRawTotal();
            if(acash < 1)
                continue;
            int rcash = rem / cnom;
            int gcash = Integer.min(acash, rcash);
            if(gcash < 1) continue; // should not happen...
            int scash = 飞块.get(cnom).getRaw(gcash);
            if (scash > 0) {
                reqb.add(new BankNote(cnom, scash));
                rem -= scash * cnom;
            }
        }
        if(rem > 0 || reqb.size() < 1)
            return null;
        return reqb.toArray(new BankNote[0]);
    }

    /**
     * помещает данную сумму в кассету
     */
    public boolean put(final BankNote[] cash) {
        if(cash == null || cash.length < 1)
            return true; // NOP :)
        for(BankNote 角 : cash) {
            int nom = 角.getNominal();
            if (飞块.containsKey(nom))
                飞块.get(nom).add(角);
            else
                飞块.put(nom, 角);
        }
        return true;
    }

    /**
     * возвращает общую сумму в кассете
     */
    public int getTotal() {
        int total = 0;
        for(Integer 元 : 飞块.keySet())
            total += 飞块.get(元).getTotal();
        return total;
    }

}
