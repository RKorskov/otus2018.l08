// -*- coding: utf-8; indent-tabs-mode: nil; word-wrap: t; mode: java; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-07-16 12:23:57 korskov>

/**
 * L7 Написать эмулятор АТМ (банкомата)
 *
 * Объект класса АТМ должен уметь:
 * 1. принимать банкноты разных номиналов (на каждый номинал должна быть
 *    своя ячейка);
 * 2. выдавать запрошенную сумму минимальным количеством банкнот или
 *    ошибку если сумму нельзя выдать;
 * 3. выдавать сумму остатка денежных средств;

 */

package L07;

import L07.BankNote;

public interface ATM {
    /**
     * помещает данную сумму в кассету ATM
     */
    public boolean put(BankNote[] cash);

    /**
     * извлекает запрошенную сумму из кассеты ATM;
     * возвращает null при невозможности выдать запрошенную сумму;
     */
    public BankNote[] get(int amount);

    /**
     * возвращает общую сумму в кассете ATM
     */
    public int getTotal();

    /**
     * (L08.ATMDepartment)
     * сброс состояния ATM до начального
     */
    public boolean reset();

    // public ATMState getState(); // ?
}
