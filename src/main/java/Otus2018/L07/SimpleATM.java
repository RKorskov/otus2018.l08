// -*- coding: utf-8; indent-tabs-mode: nil; word-wrap: t; mode: java; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-07-16 12:24:11 korskov>

package L07;

/**
 * реализация тривиального (как машина Тьюринга) ATM
 */

import java.util.Arrays;

import L07.ATM;
import L07.BankNote;
import L07.CashBox;

public class SimpleATM implements ATM {
    private CashBox 红包;
    private final BankNote[] 百包;

    public SimpleATM() {
        红包 = new SimpleCashBox();
        百包 = null;
    }

    public SimpleATM(final BankNote[] cash1st) {
        红包 = new SimpleCashBox(cash1st);
        百包 = Arrays.copyOf(cash1st, cash1st.length);
    }

    /**
     * (L08.ATMDepartment)
     * сброс состояния ATM до начального
     */
    public boolean reset() {
        红包 = new SimpleCashBox(百包);
        return true;
    }

    /**
     * извлекает запрошенную сумму из кассеты ATM;
     * возвращает null при невозможности выдать запрошенную сумму;
     */
    public BankNote[] get(final int amount) {
        return 红包.get(amount);
    }

    /**
     * помещает данную сумму в кассету ATM
     */
    public boolean put(final BankNote[] cash) {
        return 红包.put(cash);
    }

    /**
     * возвращает общую сумму в кассете ATM
     */
    public int getTotal() {
        return 红包.getTotal();
    }

}
