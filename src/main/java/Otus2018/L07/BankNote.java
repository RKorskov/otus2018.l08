// -*- coding: utf-8; indent-tabs-mode: nil; word-wrap: t; mode: java; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-07-16 11:04:02 korskov>

package L07;

/**
 * Описание кучки банкнот одного номинала:
 * 1. номинал (ячейка кассеты ATM);
 * 2. (части) выдаваемой суммы;
 * 3. (части) вносимой суммы;
 */

public class BankNote {
    private final int nominal;
    private int amount;

    /**
     * @param nominal -- nominal value of this kind of banknotes
     */
    public BankNote(int nominal) {
        this.nominal = nominal;
        this.amount = 0;
    }

    /**
     * @param amount -- raw amount of banknotes
     */
    public BankNote(int nominal, int amount) {
        this(nominal);
        this.amount = amount;
    }

    public int getNominal() {
        return nominal;
    }

    public int getTotal() {
        return amount * nominal;
    }

    protected int getRawTotal() {
        return amount;
    }

    /**
     * @param: amount  запрошеная сумма без привязки к номиналу
     * (e.g. 127元 при номинале 64块)
     * @return: nominal * (amount // nominal)
     */
    public int get(final int amount) {
        int areq = amount / nominal;
        if(amount < 1 || areq > this.amount)
            return 0;
        this.amount -= areq;
        return areq * this.nominal;
    }

    /**
     * @param: amount  запрошеная сумма без привязки к номиналу
     * (e.g. 127元 при номинале 64块)
     * @return: nominal * (amount // nominal)
     */
    public int getRaw(final int amount) {
        if(amount < 1 || amount > this.amount)
            return 0;
        this.amount -= amount;
        return amount;
    }

    /**
     * явно выставляет занчение this.amount в (amount // nominal)
     */
    protected boolean set(final int amount) {
        int aam = amount / this.nominal;
        if(aam < 0)
            return false;
        this.amount = aam;
        return true;
    }

    /**
     * явно устанавливает this.amount,
     * номиналы должны совпадать
     */
    protected boolean setRaw(final BankNote amount) {
        if(amount == null)
            return true;
        if(amount.getNominal() != nominal || amount.getRawTotal() < 0)
            return false;
        this.amount = amount.getRawTotal();
        return true;
    }

    /**
     * добавляет указанное количество денежных единиц в кучку,
     * добавляется кратное nominal количество денежных единиц,
     * но не большее, чем amount // nominal
     * некратный остаток игнорируется
     * e.g.: 19 для 384 при номинале 20
     * т.е. amount должно быть кратно nominal
     */
    public boolean add(final int amount) {
        final int aam = amount / this.nominal;
        if(aam < 1)
            return false;
        this.amount += aam;
        return true;
    }

    /**
     * добавляет указанное количество банкнот в данную кучу
     * null aka NOP,
     * разный номинал это ошибка
     */
    public boolean add(final BankNote amount) { // a.k.a. addRaw
        if(amount == null)
            return true; // NOP
        if(amount.getNominal() != nominal || amount.getRawTotal() < 1)
            return false;
        this.amount += amount.getRawTotal();
        return true;
    }
}
