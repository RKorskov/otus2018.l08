// -*- coding: utf-8; indent-tabs-mode: nil; word-wrap: t; mode: java; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-07-29 13:27:15 korskov>

package L08;

/**
 * <h3>L8 ATM Department</h3>
 * <p>
 * Написать приложение ATM Department:
 * <ol>
 * <li> Приложение может содержать несколько ATM;
 * <li> Departmant может собирать сумму остатков со всех ATM;
 * <li> Department может инициировать событие – восстановить состояние
 * всех ATM до начального (начальные состояния у разных ATM могут быть
 * разными);
 * </ol>
 */

import L07.ATM;
import L07.SimpleATM;
import L07.BankNote;

import java.util.ArrayList;

public class ATMDepartment {
    private ArrayList <ATM> atms;

    public boolean addATM() {
        atms.add(new SimpleATM());
        return true;
    }

    public boolean addATM(final BankNote[] cashIn) {
        atms.add(new SimpleATM(cashIn));
        return true;
    }

    public void resetAll() {
        for(ATM atm : atms)
            atm.reset();
        return;
    }

    public long collectLeftoversAll() {
        long ra = 0;
        for(ATM atm : atms)
            ra += atm.getTotal();
        return ra;
    }
}
