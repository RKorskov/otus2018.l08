// -*- coding: utf-8; indent-tabs-mode: nil; word-wrap: t; mode: java; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-07-16 12:45:13 korskov>

package Otus2018.L08;

// JUnit 4.x
import org.junit.*;
import static org.junit.Assert.*;

import L07.BankNote;

public class TestSuite_BankNote {
    private final static int __NOMINAL = 7;

    @Test
    public void createNew() {
        BankNote bn = new BankNote(__NOMINAL);
        assertTrue(bn.getNominal() == __NOMINAL);
    }

    @Test
    public void createNew_withCash() {
        final int rawAmountIn = 3;
        final int cashIn = __NOMINAL * rawAmountIn;
        BankNote bn = new BankNote(__NOMINAL, rawAmountIn);
        assertTrue(bn.getNominal() == __NOMINAL);
        assertTrue(bn.getTotal() == cashIn);
    }

    @Test
    public void addCash() {
        final int rawAmountIn = 5;
        final int cashIn = __NOMINAL * rawAmountIn;
        BankNote bn = new BankNote(__NOMINAL);
        bn.add(cashIn);
        assertTrue(bn.getTotal() == cashIn);
    }

    @Test
    public void addCashTwice() {
        final int rawAmountIn0 = 5,
            rawAmountIn1 = 6;
        final int cashIn0 = __NOMINAL * rawAmountIn0,
            cashIn1 = __NOMINAL * rawAmountIn1;
        BankNote bn = new BankNote(__NOMINAL);
        bn.add(cashIn0);
        bn.add(cashIn1);
        assertTrue(bn.getTotal() == (cashIn0 + cashIn1));
    }

    @Test
    public void addCash_and_getCashAll() {
        final int rawAmountIn = 5;
        final int cashIn = __NOMINAL * rawAmountIn;
        BankNote bn = new BankNote(__NOMINAL, rawAmountIn);
        assertTrue(bn.get(cashIn) == cashIn);
    }

    @Test
    public void addCash_and_getCashPart() {
        final int rawAmountIn = 5,
            rawAmountOut = 2;
        final int cashIn = __NOMINAL * rawAmountIn,
            cashOut = __NOMINAL * rawAmountOut;
        BankNote bn = new BankNote(__NOMINAL, rawAmountIn);
        assertTrue(bn.get(cashOut) == cashOut);
        assertTrue(bn.getTotal() == (cashIn - cashOut));
    }
}
