// -*- coding: utf-8; indent-tabs-mode: nil; word-wrap: t; mode: java; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-07-16 12:45:07 korskov>

package Otus2018.L08;

// JUnit 4.x
import org.junit.*;
import static org.junit.Assert.*;

import L07.ATM;
import L07.SimpleATM;
import L07.BankNote;

public class TestSuite_ATM {
    /**
     * newly created ATM have nothing in it
     */
    @Test
    public void test_newATMisEmpty() {
        ATM atm = new SimpleATM();
        assertTrue(atm.getTotal() == 0);
    }

    @Test
    public void test_putCashIn() {
        ATM atm = new SimpleATM();
        BankNote bns[] = {
            new BankNote(1),
            new BankNote(2),
            new BankNote(4),
            new BankNote(8)
        };
        bns[0].add(3);
        bns[1].add(6);
        bns[2].add(12);
        bns[3].add(32);
        atm.put(bns);
        assertTrue(atm.getTotal() == 53);
    }

}
