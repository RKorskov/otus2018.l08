package Otus2018.L08;

// JUnit 4.x
// https://howtodoinjava.com/junit-4/
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
 
@Suite.SuiteClasses({
   TestSuite_ATM.class,
   TestSuite_BankNote.class
})

public class AppTest {}
